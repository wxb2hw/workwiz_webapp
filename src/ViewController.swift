

import UIKit
import WebKit

class ViewController: UIViewController, WKUIDelegate, WKNavigationDelegate, WKScriptMessageHandler, UITabBarDelegate {
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    var webViews = [WKWebView]()
    var topWebView: WKWebView {
        get {return self.webViews.last!}
    }

    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var webViewsContainer: UIView!
    
    @IBAction func onBackButtonClick(_ sender: UIButton) {
        if let topWebView = self.webViews.last {
            if topWebView.canGoBack {
                topWebView.goBack()
            } else if topWebView != self.webViews.first {
                self.webViews.popLast()?.removeFromSuperview()
            }
        }
    }
    
    @IBAction func onHomeTap(_ sender: UIButton) {
        self.open(url: "https://workwiz.co.kr/")
    }
    @IBAction func onJobTap(_ sender: UIButton) {
        self.open(url: "https://workwiz.co.kr/job")
    }
    @IBAction func onCoachTap(_ sender: UIButton) {
        self.open(url: "https://workwiz.co.kr/coach")
    }
    @IBAction func onRecruitTap(_ sender: UIButton) {
        self.showAlert()
//        self.open(url: "https://workwiz.co.kr/recruit")
    }
    @IBAction func onMyTap(_ sender: UIButton) {
        self.open(url: "https://workwiz.co.kr/my/favorite")
    }
    
    @IBOutlet weak var TabBar: UITabBar!
    @IBOutlet weak var homeTab: UITabBarItem!
    @IBOutlet weak var jobTab: UITabBarItem!
    @IBOutlet weak var recruitTab: UITabBarItem!
    @IBOutlet weak var couchTab: UITabBarItem!
    @IBOutlet weak var myPageTab: UITabBarItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.TabBar.delegate = self
        self.TabBar.selectedItem = self.TabBar.items?.first
        
        let webConfiguration = WKWebViewConfiguration()
        webConfiguration.applicationNameForUserAgent = "Workwiz iOS"
        createWebView(frame: self.webViewsContainer.bounds, configuration: webConfiguration).isHidden = true

        backButton.layer.borderWidth = 1
        backButton.isHidden = false
        backButton.layer.borderColor = UIColor.lightGray.cgColor
        
        progressView.isHidden = true
    
        // Do any additional setup after loading the view.
    }
    
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        switch item {
        case homeTab:
            self.open(url: "https://workwiz.co.kr/")
            break
        case jobTab:
            self.open(url: "https://workwiz.co.kr/job")
            break
        case recruitTab:
            self.open(url: "https://workwiz.co.kr/recruit")
            break
        case couchTab:
            self.open(url: "https://workwiz.co.kr/coach")
            break
        case myPageTab:
            self.open(url: "https://workwiz.co.kr/my/favorite")
            break
        default:
            print("Unknown TabBarItem: ")
            print(item)
        }
    }
    
    func createWebView(frame: CGRect, configuration: WKWebViewConfiguration) -> WKWebView {
        let webView = WKWebView(frame: frame, configuration: configuration)
        
        webView.customUserAgent = "Workwiz iOS"
//      webView.customUserAgent = "\(result) app_ios"
//      webView.configuration.userContentController.add(self,name: "scriptHandler")
        webView.configuration.preferences.javaScriptEnabled = true
        webView.addObserver(self, forKeyPath: #keyPath(WKWebView.estimatedProgress), options: .new, context: nil)

        // set delegate
        webView.uiDelegate = self
        webView.navigationDelegate = self
        
        webView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        webView.allowsBackForwardNavigationGestures = true
        
        self.webViewsContainer.addSubview(webView)
        self.webViews.append(webView)
        
        return webView
    }
    
    func open(url: String) {
        self.webViews.last?.isHidden = false
        self.webViews.last?.load(URLRequest(url: URL(string: url)!))
    }
    
    /// ---------- 팝업 열기 ----------
    /// - 카카오 JavaScript SDK의 로그인 기능은 popup을 이용합니다.
    /// - window.open() 호출 시 별도 팝업 webview가 생성되어야 합니다.
    ///
    func webView(_ webView: WKWebView, createWebViewWith configuration: WKWebViewConfiguration, for navigationAction: WKNavigationAction, windowFeatures: WKWindowFeatures) -> WKWebView? {
        guard let frame = self.webViews.last?.frame else {
            return nil
        }
        return createWebView(frame: frame, configuration: configuration)
    }
    
    /// ---------- 팝업 닫기 ----------
    /// - window.close()가 호출되면 앞에서 생성한 팝업 webview를 닫아야 합니다.
    ///
    func webViewDidClose(_ webView: WKWebView) {
        self.webViews.popLast()?.removeFromSuperview()
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "estimatedProgress" {
            if (topWebView.estimatedProgress == 1.0) {
                progressView.isHidden = true
            } else {
                progressView.isHidden = false
            }
            progressView.progress = Float(topWebView.estimatedProgress)
        }
    }
    
    @available(iOS 8.0, *)
    
    public func webView(_ webView: WKWebView, runJavaScriptAlertPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping () -> Swift.Void){
        
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        
        let otherAction = UIAlertAction(title: "OK", style: .default, handler: {action in completionHandler()})
        
        alert.addAction(otherAction)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    public func showAlert() {
        let alertController = UIAlertController(title: "알림",
                           message: "인재를 찾고 계신가요?\n워크위즈 기업회원을 위한 서비스입니다.\n아직 회원이 아니시라면,[기업회원]가입 인재를 찾아보세요!", preferredStyle: .alert)
           let cancelAction = UIAlertAction(title: "닫기", style: .cancel, handler: nil)
           let okAction = UIAlertAction(title: " 기업회원 가입", style: .default, handler: {
               action in
            self.open(url: "https://workwiz.co.kr/my/favorite")
           })
           alertController.addAction(cancelAction)
           alertController.addAction(okAction)
           self.present(alertController, animated: true, completion: nil)
    }
    
    
    
    @available(iOS 8.0, *)
    
    public func webView(_ webView: WKWebView, runJavaScriptConfirmPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping (Bool) -> Swift.Void){
        
        let alert = UIAlertController(title: "", message: message, preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "CANCEL", style: .cancel, handler: {(action) in completionHandler(false)})
        
        let okAction = UIAlertAction(title: "OK", style: .default, handler: {(action) in completionHandler(true)})
        
        alert.addAction(cancelAction)
        
        alert.addAction(okAction)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    
    
    
    
    @available(iOS 8.0, *)
    
    public func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!){
        
//        activityIndicator = UIActivityIndicatorView(style: .whiteLarge)
//
//        activityIndicator.frame = CGRect(x: view.frame.midX-50, y: view.frame.midY-50, width: 100, height: 100)
//
//        activityIndicator.color = UIColor.red
//
//        activityIndicator.hidesWhenStopped = true
//
//        activityIndicator.startAnimating()
//
//        self.view.addSubview(activityIndicator)
        
    }
    
    @available(iOS 8.0, *)
    public func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!){
        
        //activityIndicator.stopAnimating()
        
//        self.activityIndicator.removeFromSuperview()
        
//        if webView.url!.absoluteString.range(of: "://workwiz.co.kr/") != nil {
//            backButton.isHidden = true
//        } else {
//            backButton.isHidden = false
//        }
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        print("absoluteString:" + (navigationAction.request.url?.absoluteString ?? ""))
        guard let url = navigationAction.request.url else {
            decisionHandler(.allow)
            return
        }
        let components = URLComponents(url: url, resolvingAgainstBaseURL: false)
        let app = UIApplication.shared
        if (components?.scheme == "http" || components?.scheme == "https") {
            decisionHandler(.allow)
            return
        }
        app.open(url)
        decisionHandler(.cancel)
    }
    
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
           // message.name = "scriptHandler" -> 위에 WKUserContentController()에 설정한 name
           // message.body = "searchBar" -> 스크립트 부분에 webkit.messageHandlers.scriptHandler.postMessage(<<이부분>>)
           if let body = message.body as? String, body == "notice" {
               guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                  return
              }

              if UIApplication.shared.canOpenURL(settingsUrl) {
                  UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                      print("Settings opened: \(success)") // Prints true
                  })
              }
               
           }
           if message.body is Array<Any> {
               print(message.body)
               
           }
           
       }
       
}

